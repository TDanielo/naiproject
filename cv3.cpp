/**
 * @file objectDetection2.cpp
 * @author A. Huaman ( based in the classic facedetect.cpp in samples/c )
 * @brief A simplified version of facedetect.cpp, show how to
 *       load a cascade classifier and how to find objects (Face + eyes)
 *       in a video stream - Using LBP here
 */
// kod zmodyfikowany na potrzeby projektu z NAI na PJATK Gdańsk


#include <cv.hpp>
#include <highgui.h>
#include <iostream>
#include <stdio.h>

using namespace std;
using namespace cv;

CascadeClassifier face_cascade;
CascadeClassifier eyes_cascade;
Mat glasses;
Mat glasses2;
/** Function Headers */
void detectAndDisplay( Mat frame );

/**
 * @function main
 */
int main( void ) {
int loRange[3] = {112,185,75};
	int hiRange[3] = {162,255,255};
	
		

	
	namedWindow("ustawienia", CV_WINDOW_AUTOSIZE);
	createTrackbar("loRange0", "ustawienia",&(loRange[0]), 255);
	createTrackbar("loRange1", "ustawienia",&(loRange[1]), 255);
	createTrackbar("loRange2", "ustawienia",&(loRange[2]), 255);
	createTrackbar("hiRange0", "ustawienia",&(hiRange[0]), 255);
	createTrackbar("hiRange1", "ustawienia",&(hiRange[1]), 255);
	createTrackbar("hiRange2", "ustawienia",&(hiRange[2]), 255);

	VideoCapture capture;
	Mat frame,imgOrig;

	//-- 1. Load the cascade
	if( !face_cascade.load( String( "lbpcascade_frontalface.xml" ) ) ) {
		printf( "--(!)Error loading face cascade\n" );
		return -1;
	};
	if( !eyes_cascade.load( String( "haarcascade_eye_tree_eyeglasses.xml" ) ) ) {
		printf( "--(!)Error loading eyes cascade\n" );
		return -1;
	};
	glasses = imread( "dwi.png", -1 );
	
	std::cout << "C:" << glasses.channels() << "\n";

	
	//-- 2. Read the video stream
	capture.open( -1 );
	if ( ! capture.isOpened() ) {
		printf( "--(!)Error opening video capture\n" );
		return -1;
	}

	while ( capture.read( frame ) ) {
		if( frame.empty() ) {
			printf( " --(!) No captured frame -- Break!" );
			break;
		}

		vector < vector < Point > > contours;
		imgOrig = frame.clone();
		cvtColor(imgOrig, imgOrig, CV_RGB2HSV);
		inRange(imgOrig, Scalar(loRange[0],loRange[1],loRange[2]),
						Scalar(hiRange[0],hiRange[1],hiRange[2]), imgOrig);

		findContours(imgOrig, contours, CV_RETR_LIST, CV_CHAIN_APPROX_NONE);

		// sordek okregu
		Point poi;
		poi.x = 100; 
		poi.y = 100;
	
		circle(frame,poi,50,cv::Scalar(0,0,255));
		// https://docs.opencv.org/2.4.13.4/doc/tutorials/imgproc/shapedescriptors/moments/moments.html
		vector<Moments> mu(contours.size() );
  for( int i = 0; i < contours.size(); i++ )
     { mu[i] = moments( contours[i], false ); }

  ///  Get the mass centers:
  vector<Point2f> mc( contours.size() );

  for( int i = 0; i < contours.size(); i++ )
     { 
		 mc[i] = Point2f( mu[i].m10/mu[i].m00 , mu[i].m01/mu[i].m00 );
		 double l = sqrt(pow(mc[i].x - poi.x,2) + pow(mc[i].y - poi.y,2)); // odl srodka "obiektu" od środka okregu
		 if(l < 5){
			 glasses = imread( "dwi2.png", -1 );
			 std::cout << "C:" << glasses.channels() << "\n";		

		 } 
	 
	 
	 }
		
		//-- 3. Apply the classifier to the frame
		detectAndDisplay( frame );
		
		imshow("ustawienia", imgOrig);
		
		//-- bail out if escape was pressed
		int b = waitKey( 1 );
		if( ( char )b == 27 ) {
			return 0;
				break;
		}
	}
	return 0;
}

// funkcja nakladajaca obraz z przezroczystoscia
// w oparciu o http://dsynflo.blogspot.in/2014/08/simplar-2-99-lines-of-code-for.html
void imageOverImageBGRA( const Mat &srcMat, Mat &dstMat, const vector<Point2f> &dstFrameCoordinates ) {
	if ( srcMat.channels() != 4 ) throw "Nakladam tylko obrazy BGRA";

	// tylko kanal alpha
	vector<Mat> rgbaChannels( 4 );
	Mat srcAlphaMask( srcMat.rows, srcMat.cols, srcMat.type() );
	split( srcMat, rgbaChannels );
	rgbaChannels = {rgbaChannels[3],rgbaChannels[3],rgbaChannels[3]};
	merge( rgbaChannels, srcAlphaMask );

	// wspolrzedne punktow z obrazu nakladanego
	vector<Point2f> srcFrameCoordinates = {{0,0},{(float)srcMat.cols,0},{(float)srcMat.cols,(float)srcMat.rows},{0,(float)srcMat.rows}};
	Mat warp_matrix = getPerspectiveTransform( srcFrameCoordinates, dstFrameCoordinates );

	Mat cpy_img( dstMat.rows, dstMat.cols, dstMat.type() );
	warpPerspective( srcAlphaMask, cpy_img, warp_matrix, Size( cpy_img.cols, cpy_img.rows ) ); // Transform a srcAlphaMask overlay image to position
	Mat neg_img( dstMat.rows, dstMat.cols, dstMat.type() );
	warpPerspective( srcMat, neg_img, warp_matrix, Size( neg_img.cols, neg_img.rows ) ); // Transform overlay Image to the position - [ITEM1]
	dstMat = dstMat - cpy_img;
	
	cvtColor(neg_img, neg_img, CV_BGRA2BGR);
	cpy_img = cpy_img / 255;
	neg_img = neg_img.mul( cpy_img );
	dstMat = dstMat + neg_img;
}


void detectAndDisplay( Mat frame ) {
	std::vector<Rect> faces;
	Mat frame_gray;

	cvtColor( frame, frame_gray, COLOR_BGR2GRAY );
	equalizeHist( frame_gray, frame_gray );
	//-- Wykrywamy twarz
	face_cascade.detectMultiScale( frame_gray, faces, 1.1, 2, 0, Size( 12, 12 ) );	

	for( size_t i = 0; i < faces.size(); i++ ) {
		Mat faceROI = frame_gray( faces[i] ); // range of interest
		//imshow ( "ROI", faceROI );
		std::vector<Rect> eyes;
		eyes_cascade.detectMultiScale( faceROI, eyes, 1.1, 2, 0 | CASCADE_SCALE_IMAGE, Size( 5, 5 ) );
		if( eyes.size() > 0 ) {
			vector<Point2f> dst;
			dst.push_back( Point2f( faces[i].x, faces[i].y + faces[i].height * 5 / 20 ) );
			dst.push_back( Point2f( faces[i].x + faces[i].width, faces[i].y + faces[i].height * 5 / 20               ) );
			dst.push_back( Point2f( faces[i].x + faces[i].width, faces[i].y + faces[i].height * 5 / 20 + faces[i].height * 3 / 10 ) );
			dst.push_back( Point2f( faces[i].x, faces[i].y + faces[i].height * 5 / 20 + faces[i].height * 3 / 10 ) );
			imageOverImageBGRA( glasses.clone(), frame, dst );
		}
	}
	cv::flip( frame, frame, 1 );

	imshow( "DWI", frame );
	
}


